mkdir -Force .devcontainer
mkdir -Force .vscode

@'
{
    "dockerComposeFile": "../scripts/compose.yml",
    "name": "PyTemplate",
    "service": "devcontainer",
    "workspaceFolder": "/workspace",
    "postStartCommand": "./scripts/initialize_devcontainer.sh",
    "customizations": {
        "vscode": {
            "extensions": [
                "ms-python.python",
                "tamasfe.even-better-toml",
                "robocorp.robotframework-lsp"
            ]
        }
    }
}
'@ | Out-File .devcontainer/devcontainer.json -Encoding "UTF8"

@'
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Module",
            "type": "python",
            "request": "launch",
            "module": "unittest",
            "args": [
                "discover",
                "tests"
            ]
        }
    ]
}
'@ | Out-File .vscode/launch.json -Encoding "UTF8"

@'
{
    "python.formatting.provider": "black",
    "python.formatOnSave": true,
    "python.linting.mypyEnabled": true,
    "python.linting.enabled": true,
    "python.linting.lintOnSave": true
}
'@ | Out-File .vscode/settings.json -Encoding "UTF8"