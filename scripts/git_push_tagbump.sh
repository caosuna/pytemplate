#!/bin/bash
RELEASE_TYPE=$1

NEW_VERSION=$(python scripts/bump_version.py $(pytemplate --version) $RELEASE_TYPE)
git tag -a ${NEW_VERSION} -m "v${NEW_VERSION}"
git push origin --tags