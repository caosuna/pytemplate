def func():
    """Returns hello world string.

    Parameters:
        None

    Returns:
        str: hello world string

    """
    return "Hello World"
