# Contributing Guide

## Directory Structure
```
{pytemplate} (project root folder)
└─── docs/sphinx (source files for generating docs)
│
└─── notebooks/ (Jupyter Notebooks for experimentation and generating visualizations/summaries)
│
└─── scripts/ (useful snippets for various toolings)
│
└─── src/ (all source code)
|   │
|    pytemplate/ (main directory of module code)
|       └─── __main__.py (Defines module call behavior)
|       └─── __init__.py (Defines top-level API)
|
└─── tests/ (tests for source code)
│   └─── test_acceptance/ (robot framework tests)
│   └─── test_pytemplate/ (tests encompassing src/pytemplate)
│   └─── * (further test files mirroring structure of src/)
│
└─── .gitlab-ci.yml (CI/CD pipeline configuration for GitLab)
└─── dockerfile (contains dev and prod environment definitions)
└─── license.txt (license information)
└─── pyproject.toml (project and tooling configuration)
```

## Setup <!-- omit in toc -->
DevEx is oriented around the use of Dev Containers. 
Set pre-configuration of VSCode on Windows with the script below.
```powershell
scripts\vscode_configure.ps1
```
Install `Remote Development` extension for VSCode and `Open Folder in Container...`.
    
## Testing
Pre-commit will perform all checks of interest that are required for CI.
```shell
root@79c1e60abb30:/workspace# ./scripts/pre-commit_run_hooks.sh
```
