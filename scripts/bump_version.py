import sys
import unittest


def main(*argv):
    print(bump_version(*argv))


def bump_version(version: str, type="patch"):
    major, minor, patch = version.split(".")[:3]
    if type == "patch":
        if len(version.split(".")) > 3:
            patch = str(int(patch))
        else:
            patch = str(int(patch) + 1)
    elif type == "minor":
        patch = "0"
        minor = str(int(minor) + 1)
    elif type == "major":
        patch = "0"
        minor = "0"
        major = str(int(major) + 1)

    return ".".join([major, minor, patch])


class Test_Bump_Version(unittest.TestCase):
    def test_patch_bump_truncates_dev_version(self):
        previous = "1.1.3.dev26+g67c414b.d20230411"
        expected = "1.1.3"
        actual = bump_version(previous, "patch")
        self.assertEqual(expected, actual)

    def test_patch_bump_increments_unappended_version(self):
        previous = "1.1.3"
        expected = "1.1.4"
        actual = bump_version(previous, "patch")
        self.assertEqual(expected, actual)

    def test_minor_bump_clears_patch_and_increments_minor(self):
        previous = "1.0.3.dev26+g67c414b.d20230411"
        expected = "1.1.0"
        actual = bump_version(previous, "minor")
        self.assertEqual(expected, actual)

    def test_major_bump_clears_minor_patch_and_increments_major(self):
        previous = "1.0.3.dev26+g67c414b.d20230411"
        expected = "2.0.0"
        actual = bump_version(previous, "major")
        self.assertEqual(expected, actual)


if __name__ == "__main__":
    main(*sys.argv[1:])
