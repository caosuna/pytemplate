.. pytemplate documentation master file, created by
   sphinx-quickstart on Mon Aug 30 10:39:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pytemplate's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pytemplate

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
