# PyTemplate <!-- omit in toc -->
[![version](https://img.shields.io/gitlab/v/tag/caosuna/pytemplate)](https://gitlab.com/caosuna/pytemplate/)
[![python-3.9](https://img.shields.io/badge/python-3.9-yellow)](https://hub.docker.com/_/python)
[![License: MIT](https://img.shields.io/badge/license-MIT-lightgrey)](https://www.gnu.org/licenses/gpl-3.0)

## Summary <!-- omit in toc -->
This project implements tooling to optimize the SDLC in as self-documenting a way as possible.

## Setup
See the `pyproject.toml` for requirement and dependency details.
```
pip install pytemplate --index-url https://gitlab.com/api/v4/projects/21620541/packages/pypi/simple
```

## Usage
The documentation for usage of this package as a CLI tool is documented in the [acceptance tests](tests/test_acceptance/cli_user.robot).
This can be used as a basic framework for [ATDD](https://en.wikipedia.org/wiki/Acceptance_test-driven_development). SDLC may look something like below on the timeline of about a day:
```mermaid
flowchart LR
    REQ["Pair/Mob Program Acceptance Test(s) with User(s)"]
    DEV["Test, Dev, Refactor until UAT Passing"]
    CICD["Run through CI/CD Pipeline"]
    REV[Get User Feedback and Review]
    REQ --> DEV --> CICD --> REV --> REQ
```

## Contribute
See the [contributing document](contributing.md) for dev setup and testing details.
