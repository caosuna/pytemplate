*** Settings ***
Documentation       An acceptance test suite for PyTemplate.
...                 These tests should be for a particular user
...                 and written in their usage and domain language.

Library             Resource.py
Variables           Resource.py

Test Teardown       Clear CLI Output


*** Test Cases ***
Running PyTemplate CLI Returns Hello World To Command Line
    When User Calls From Command Line    pytemplate
    Then Command Line Should Print    Hello World

Running PyTemplate CLI With Version Flag Returns Version To Command Line
    When User Calls From Command Line    pytemplate --version
    Then Command Line Should Print    ${PyTemplate Version}
    And String is SemVerish    ${PyTemplate Version}
