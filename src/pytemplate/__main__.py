import argparse  # pragma: no cover

from . import script  # pragma: no cover
from . import __version__


def main():
    parse_args()
    print(script.func())


def parse_args():
    parser = argparse.ArgumentParser(
        description="Run pytemplate module",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        help="returns version string",
        version=__version__,
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":  # pragma: no cover
    main()
