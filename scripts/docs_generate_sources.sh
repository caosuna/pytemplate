#!/bin/bash
sphinx-quickstart docs -q -p pytemplate -a "Carlos Osuna" --ext-autodoc --ext-todo --ext-viewcode
rm docs/modules.rst
sphinx-apidoc -o docs/sphinx src -M