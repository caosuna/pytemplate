#!/bin/bash
DOCKER_USER=$1
DOCKER_PASSWORD=$2
DOCKER_REGISTRY=$3
GIT_HASH=$4

docker login -u $DOCKER_USER -p $DOCKER_PASSWORD $DOCKER_REGISTRY
docker compose -f ./scripts/compose.yml build
docker tag $DOCKER_REGISTRY/devcontainer:latest $DOCKER_REGISTRY/devcontainer:$GIT_HASH
docker push $DOCKER_REGISTRY/devcontainer --all-tags