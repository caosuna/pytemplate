# Notebooks
For process documentation, experimentation, and visualization.
If it is dependent on the package, it should report the version used via printing of __version__.

## Examples Group
- [Example Name](example.ipynb)
