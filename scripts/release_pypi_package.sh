#!/bin/bash
REPO_URL=$1
REPO_TOKEN=$2

python -m build --wheel
twine upload --repository-url $REPO_URL dist/* -u gitlab-ci-token -p $REPO_TOKEN