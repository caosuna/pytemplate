FROM python:3.9 as base
SHELL ["/bin/bash", "-c"]
# ENV QT_QPA_PLATFORM=offscreen # Qt virtual rendering
# RUN apt-get update && apt-get install libgl1 -y # for graphics rendering
RUN mkdir -p dependencies/src
COPY pyproject.toml dependencies/pyproject.toml
RUN pip install ./dependencies

FROM base as dev
RUN pip install ./dependencies[dev]
RUN rm -rf dependencies
CMD ["sleep", "infinity"]

FROM dev as package
COPY . /package
WORKDIR /package
RUN python -m build --wheel

FROM base as prod
RUN rm -rf dependencies
COPY --from=package /package/dist dist
RUN pip install `ls -d dist/*.whl`
RUN rm -R dist