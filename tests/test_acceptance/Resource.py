from robot.libraries.BuiltIn import BuiltIn
import os

import pytemplate


class Resource:
    """This is a user written keyword library.
    These libraries can be pretty handy for more complex tasks and typically
    more efficient to implement compare to Resource files.

    """

    robot_library_scope = "GLOBAL"
    pytemplate_version = pytemplate.__version__
    _output_file = "output.log"
    command_line_output = None

    def user_calls_from_command_line(self, command: str):
        os.system(f"{command} > {self._output_file}")
        with open(self._output_file) as file:
            contents = file.read()
        os.remove(self._output_file)
        if contents.endswith("\n"):
            contents = contents[:-1]
        self.command_line_output = contents
        return contents

    def string_is_semverish(self, string: str):
        split_string = string.split(".")
        return BuiltIn().should_be_true(
            len(split_string) >= 3
            and split_string[0].isnumeric()
            and split_string[1].isnumeric()
            and split_string[2].isnumeric(),
            f"Not SemVer: {string}",
        )

    def command_line_should_print(self, string: str):
        return BuiltIn().should_be_true(
            self.command_line_output == string, str(self.command_line_output)
        )

    def clear_cli_output(self):
        self.command_line_output = None
