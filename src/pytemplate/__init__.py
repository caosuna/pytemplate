from importlib.metadata import version

__version__ = version("pytemplate")

from .script import func
